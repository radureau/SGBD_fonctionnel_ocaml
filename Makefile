CC=ocamlopt
EXEC=main

MLI_FILES = $(shell find src/ -type f -name '*.mli')
ML_FILES = $(shell find src/ -type f -name '*.ml')
CMI_FILES = $(MLI_FILES:.mli=.cmi)
CMX_FILES = $(ML_FILES:.ml=.cmx)

all: clean exe

sujet :
	mkdir Sujet
	wget -nd -P Sujet -nv -r http://www.ensiie.fr/~forest/IPF/projet/ipf_projet_2016.html

%.cmi: %.mli
	$(CC) -c $^
	@mv $@ bin

%.cmx: %.ml
	$(CC) -I bin -c $^
	@mv $@ bin
	@mv $*.o bin

mk_bin:
	@mkdir -p bin
build_mli : mk_bin $(CMI_FILES)
build_ml : build_mli $(CMX_FILES)
exe : build_ml main.cmx
	@mv main.[^m]* bin
	ocamlopt -I bin bin/*.cmx -o bin/exe
	bin/exe

clean:
	rm -rf bin

open Arbre;;

try
  let m = empty in
  let m = add "pk" "value" m in
  let _ =fold (fun key value acc -> Printf.printf "m[%s]=%s\n" key value) m () in
  let _ = Printf.printf "Hello world!\n" in
  let t = Db.create_table ["a";"b"] "a" in
  let ct = Db.Unique(["b"]) in
  let t = Db.add_constraint "U_b" ct t in
  let _ = assert (Db.column_exists "a" t) in
  let _ = assert (not (Db.column_exists "c" t)) in
  let _ = assert (Db.pk t = "a") in
  let t = Db.insert ["a","valeur de a";"b","valeur de b"] t in
  let t = Db.insert ["a","2° valeur de a";"b","2° valeur de b"] t in
  let t = Db.insert ["a","3° valeur de a";] t in
(*  let t = Db.insert ["a","4° valeur de a";] t in*)
  let ls = Db.datas t in
  let ls = Db.select (Some(["a";"b"])) ls [("a",Db.Equal("2° valeur de a"))] in
  let _ = Db.fold_line_set
    (
      fun schema line acc ->
	Db.fold_line (fun value acc -> match value with Db.Value(v) -> Printf.printf "%s\n" v | _->()) line ()
    )
    ls
    ()
  in
  ()
with
| Db.Error(e) ->
   let s = Db.string_of_error e in
   Printf.printf "%s\n" s

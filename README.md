# IPF - S3 - ENSIIE
## SGBD - ocaml

### Getting Started

#### Compiler les fichiers *.ml *.mli & Executer un fichier de test
```
$> make
```

#### Télécharger le sujet
```
$> make sujet
```

#### Structure de fichiers

     .
     ├── main.ml		<- fichier test
     ├── Makefile		<- $>make
     ├── README.md		<- manuel
     └── src			<- fichiers sources
         ├── Arbre		<- package arbre prefixe
	 │   ├── arbre.ml	<- implementation arbre prefixe
         │   └── arbre.mli	<- interface arbre prefixe
	 ├── db.ml		<- implementation base de données
	 └── db.mli		<- interface base de données


### Tools
- http://mon.univ-montp2.fr/courses/GLIN101/document/Installation_de_OCaml/Installation_OCaml.html

## Sujet

### Dates

Projet à rendre pour le vendredi **13/01/2017** à **23h59**, aucun retard ne sera toléré.  
Des soutenances pourront être organisées ensuite.  

Lire tout le sujet (tout ? tout).  

Un rendu de projet comprend :

*   Un rapport précisant et justifiant vos choix (de structures, etc.), les problèmes techniques qui se posent et les solutions trouvées ; il donne en conclusion les limites de votre programme. Le rapport sera de préférence composé avec LaTeX. Le soin apporté à la grammaire et à l'orthographe est largement pris en compte.
*   Un manuel d'utilisation, même minimal.
*   Un code _abondamment_ commenté ; la première partie des commentaires comportera systématiquement les lignes :  

1.  <tt>@requires</tt> décrivant les pré-conditions : c'est-à-dire conditions sur les paramètres pour une bonne utilisation (**pas de typage ici**),
2.  <tt>@ensures</tt> décrivant la propriété vraie à la sortie de la fonction lorsque les pré-conditions sont respectées, le cas échéant avec mention des comportements en cas de succès et en cas d'échec,
3.  <tt>@raises</tt> énumérant les exceptions éventuellement levées (et précisant dans quel(s) cas elles le sont).

Avez-vous lu tout le sujet ?

* * *

#### Protocole de dépôt

Vous devez rendre :

*   votre rapport au format <tt>pdf</tt>
*   vos fichiers de code.

rassemblés dans une archive tar gzippée identifiée comme _votre_prénom_votre_nom_<tt>.tgz</tt>.  
La commande devrait ressembler à :  
<tt>tar cvfz randolph_carter.tgz rapport.pdf fichiers.ml autres_truc_éventuels</tt>…

**Lisez le man** et testez le contenu de votre archive. Une commande comme par exemple :  
<tt>tar tvf randolph_carter.tgz</tt>  
doit lister les fichiers et donner leur taille.  
Une archive qui ne contient pas les fichiers demandés ne sera pas excusable. Une archive qui n'est pas au bon format ne sera pas considérée.

**Procédure de dépôt**  
Vous devez enregistrer votre archive tar dans le dépôt dédié au cours IPF (ipf-2016) en vous connectant à <tt>http://exam.ensiie.fr</tt>. Ce dépôt sera ouvert jusqu'au 13 janvier inclus.

* * *

### Contexte

Le but de ce projet est de mettre en place un mini _SGBD relationnel_.

Ce SGBD sera au minimum capable de gérer la création d'une table, l'ajout de contraintes basiques sur une table, l'insertion dans une table et une version allégée de l'opération de sélection.

Sans présumer (pour l'instant) des types de données acceptés par notre SGBD, une table peut être vu comme regroupant au minimum (mais pas forcément exclusivement) :

*   le schéma de la table (une liste de nom de colonnes le cas échéant accompagnés de leur types),
*   une clef primaire (un nom de colonne appartenant au schéma),
*   un ensemble de contraintes nommées,
*   une association des valeurs de clef primaire vers les lignes de la table.

Nous nous restreindrons à deux type de contraintes seulement :

*   l'unicité d'une liste de colonnes,
*   la non nullité d'une colonne.

De même, nous nous restreindrons à l'insertion dans une table (pas de risque de _trigger_).

L'insertion dans une table ne devra **en aucun cas** dépendre du nombre d'entrées dans la table.

Dans un soucis d'économie de mémoire, seules les valeurs non nulles seront explicitement stockées dans l'association

Pour une ligne donnée, la sélection d'une colonne ne devra pas dépendre du nombre de colonnes présentes dans le schéma de la table

**Vous devez au moins réaliser la première époque.**

Lire les interfaces des modules Ocaml _String_ et _Map_ est conseillé. Vous êtes libre d'utiliser les modules présents dans la bibliothèque standard de _OCaml_ aussi longtemps que vous n'utilisez (explicitement) pas de traits impératifs.

* * *

### Première époque

Pour cette première partie nous nous restreindrons de plus à un seul type de données pour nos valeurs : les chaînes de caractères (de taille variable non bornée).

1.  Proposer une structure de données **pertinente** pour représenter une ligne de notre table.
2.  Faire de même pour l'association des clefs primaires (forcement des string) vers les lignes (soit une collection de lignes).
3.  Faire de même pour les tables en général.
4.  Implanter le corps du module [Db](db.mli)

* * *

### Deuxième époque

À venir...

### Merci pour ce sujet...

Vous pouvez remercier (comme moi) M. Mouilleron pour toute l'aide qu'il m'a apporté dans l'élaboration de ce sujet (et de ce remerciement).
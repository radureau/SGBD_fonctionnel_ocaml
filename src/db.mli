(** Module de gestion d'un mini SGDB *)


type column_name = string

type schema = column_name list

(**
  Une valeur est soit NULL soit un élément de type ['a].
*)                          
type 'a value =
  | Value of 'a
  | Null


                                          
type table

type error

exception Error of error

val string_of_error : error -> string


(**
  [create_table sc pk] retourne une table vide (sans ligne) dont le
  schema est [sc] et la clef primaire est [pk].
 
  Lève [Error ...] si la clef n'appartient pas au schéma.
*)
val create_table : schema -> column_name -> table



type constraint_type =
  | Unique of column_name list
  | Not_null of column_name
                                        
(**
  [add_constraint cname ctype tbl] ajoute la contrainte de nom [cname]
  et de type [ctype] à la table [tbl].

  Lève [Error ...] si la contrainte [ctype] n'est pas implantable
  (colonne inexstante dans le schéma de la table,...) ou si la table
  [tbl] n'est pas vide.
*)
val add_constraint : string -> constraint_type -> table -> table 

                                           
(**
  [column_exists cn tbl] vérifie l'existence de la colonne [cn] dans
  la table [tbl]. 
*)
val column_exists : column_name -> table -> bool

val schema : table -> schema
                        
val pk : table -> column_name


(** 
  Une ligne représente une collection de couples
  (nom_de_colonne,valeurs).
 *)
type line 

(** 
  [fold_line f l v0] applique f à tous les membres de la collection
  [l] à la manière d'un fold.

  L'ordre n'est pas spécifié.
*)

val fold_line : (string value -> 'a -> 'a) -> line -> 'a -> 'a

(**
  Un [line_set] représente une collection de [line] et un schéma
  (commun à toutes les lignes).
*)
type line_set  

(** 
  [fold_line_set f ls v0] applique f à tous les membres de la
  collection [ls] à la manière d'un fold.

  L'ordre n'est pas spécifié.
*)
val fold_line_set : (schema -> line -> 'a -> 'a  ) -> line_set -> 'a -> 'a 
                       
val datas : table -> line_set
                    
type restrict =
  | Equal of string
  | IsNull 

(**
  [select sco data rest] implante la sélection sur le schéma [sco] (si
  None alors [*], sinon les colonnes données dans le Some sur la
  collection de lignes [data] en appliquant les restrictions [rest]

  Lève [Error ...] en cas de problème à définir précisément dans le
  rapport.
*)

val select :
  (column_name list) option -> 
  line_set ->
  (column_name*restrict) list ->
  line_set


type values_to_insert = (column_name*string) list
                                             
(** 
  [insert values tbl] insère la ligne correspondant à [values] dans la
  table [tbl] en vérifiant les contraintes déclarées sur [tbl] et
  retourne la nouvelle table.

  Lève [Error ...] en cas de problème à définir précisément dans le
  rapport.
*)
val insert : values_to_insert -> table -> table


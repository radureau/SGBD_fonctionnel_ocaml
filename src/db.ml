(** Module de gestion d'un mini SGDB *)

type column_name = string

type schema = column_name list

(**
  Une valeur est soit NULL soit un élément de type ['a].
*)
type 'a value =
  | Value of 'a
  | Null

(** 
  Une ligne représente une collection de couples
  (nom_de_colonne,valeurs).
 *)
type line = (string value)Arbre.t

type constraint_type =
  | Unique of column_name list
  | Not_null of column_name
                                          
type table = schema * column_name * (line)Arbre.t * (string*constraint_type)list

type error = string*string

exception Error of error

(**
@requires
@ensures retourne le message d'erreur associé à une exception
@raises

val string_of_error : error -> string
*)
let string_of_error err =
  let name,message = err in
  "Erreur '" ^ name ^ "' :\n\t" ^ message ^ "\n"


(**
@requires la clé appartient au schéma
@ensures 
   [create_table sc pk] retourne une table vide (sans ligne) dont le
   schema est [sc] et la clef primaire est [pk].
@raises [Error "PK not found" ...] si la clef n'appartient pas au schéma.

val create_table : schema -> column_name -> table
*)
let create_table sc pk =
  if List.mem pk sc
  then sc,pk,Arbre.empty,[]
  else raise (Error ("PK not found","The indicated primary key '"^pk^"' was not found in the given schema."))
                                        

(**
@requires
   la table [tbl] est vide
   la contrainte [ctype] concernent uniquement des noms de colonnes de [tbl]
   le nom de containte [cname] n'est pas déjà utilisé dans la table [tbl]
@ensures
   [add_constraint cname ctype tbl] ajoute la contrainte de nom [cname]
   et de type [ctype] à la table [tbl].
@raises
   Lève [Error "Cannod add constraint" ...] si
   - la contrainte [ctype] n'est pas implantable
   (colonne inexistante dans le schéma de la table
   ou nom de contrainte déjà prise)
   - la table [tbl] n'est pas vide

  val add_constraint : string -> constraint_type -> table -> table 
*)
let add_constraint cname ctype (sc,pk,a,lct) =
  let has_cols = match ctype with
    | Not_null(col) -> List.mem col sc
    | Unique(lcol) ->
       List.for_all
	 (fun col -> List.mem col sc)
	 lcol
  in
  if not has_cols
  then raise (Error ("Cannot add constraint","An associated column name of the specified constraint does not match any column in the schema of the provided table."))
  else if not( (try Some(List.assoc cname lct) with Not_found -> None)=None )
  then raise (Error ("Cannot add constraint","A constraint with the name '"^cname^"' was already added."))
  else if not (Arbre.is_empty a)
  then raise (Error ("Cannot add constraint","The provided table is already filled with lines."))
  else sc,pk,a,(cname,ctype)::lct
                                           

(**
@requires
@ensures 
   [column_exists cn tbl] vérifie l'existence de la colonne [cn] dans la table [tbl].
   retourne true si existence vérifiée, false sinon
@raises 

   val column_exists : column_name -> table -> bool
*)
let column_exists cn (sc,_,_,_) = List.mem cn sc

(**
@requires
@ensures retourne le schema de la table
@raises
val schema : table -> schema
*)
let schema (sc,_,_,_) = sc

(**
@requires
@ensures retourne le nom de colonne servant de primary key de la table
@raises

   val pk : table -> column_name
*)
let pk (_,pk,_,_) = pk

(**
@requires
@ensures
   [fold_line f l v0] applique f à tous les membres de la collection
   [l] à la manière d'un fold.
   L'ordre n'est pas spécifié.
@raises

  val fold_line : (string value -> 'a -> 'a) -> line -> 'a -> 'a
*)
let fold_line f l v0 =
  Arbre.fold
    ( fun key value acc -> f value acc ) 
    l
    v0
  
(**
  Un [line_set] représente une collection de [line] et un schéma
  (commun à toutes les lignes).
*)
type line_set = schema*(line list)

(**
@requires
@ensures
   [fold_line_set f ls v0] applique f à tous les membres de la
   collection [ls] à la manière d'un fold.
   L'ordre n'est pas spécifié.
@raises

  val fold_line_set : (schema -> line -> 'a -> 'a  ) -> line_set -> 'a -> 'a 
*)
let fold_line_set f (sc,lines) v0 =
  List.fold_left
    (fun acc line -> f sc line acc)
    v0
    lines

(**
@requires
@ensures retourne le line_set correspondant aux entrées de la table
@raises

val datas : table -> line_set
*)
let datas (sc,_,a,_) =
  let lines =
    Arbre.fold
      (fun pk line acc -> line::acc )
      a
      []
  in
  sc,lines
  
                    
type restrict =
  | Equal of string
  | IsNull 

(**
@requires
@ensures
   [select sco data rest] implante la sélection sur le schéma [sco] (si
   None alors [*], sinon les colonnes données dans le Some sur la
   collection de lignes [data] en appliquant les restrictions [rest]
@raises
   [Error "Unknown Column Name" ...] si
   - une colonne selectionnée (ie dans [sco]) n'est pas comprise dans le schema [sc] du line set
   - une colonne utilisée dans les restrictions [rest] n'est pas dans le schema [sc] du line set

   val select :
   (column_name list) option -> 
   line_set ->
   (column_name*restrict) list ->
   line_set
*)

let select sco (sc,data) rest =
  let sco' = match sco with None -> sc | Some(sco') -> sco' in

  let _ = List.fold_left (fun acc cn -> if List.mem cn sc then true else raise (Error ("Unknown Column Name","Selected column : '"^cn^"'."))) true sco' in

  let _ = List.fold_left (fun acc (cn,r) -> if List.mem cn sc then true else raise (Error ("Unknown Column Name","Restricted column : '"^cn^"'."))) true rest in
  
  List.fold_left
    (
      fun (sco',result) line ->
	let respect_restrictions =
	  List.fold_left
	    (
	      fun acc (cn,r) ->
		let a_value = try Arbre.find cn line with Not_found -> Null in
		match r,a_value with
		| Equal(s),Value(value) -> acc && value=s
		| IsNull,Value(_) -> false
		| _,_ -> acc
	    )
	    true
	    rest
	in
	if respect_restrictions
	then
	  if sco=None
	  then (sco',line::result)
	  else
	    let selection =
	      List.fold_left
		(
		  fun selection cn ->
		    try
		      let value = Arbre.find cn line in
		      Arbre.add cn (value) selection
		    with Not_found -> selection
		)
		Arbre.empty
		sco'
	    in
	    (sco',selection::result)
	else (sco',result)
    )
    (sco',[])
    data
  
         

type values_to_insert = (column_name*string) list

exception Impossible
(**
@requires
@ensures
   [insert values tbl] insère la ligne correspondant à [values] dans la
   table [tbl] en vérifiant les contraintes déclarées sur [tbl] et
   retourne la nouvelle table.
@raises
   - [Error "PK not inserted" ...] si aucune valeur pour la clé primaire n'est proposée
   - [Error "PK integrity error" ...] si une valeur identique à celle proposée a déjà été utilisée
   - [Error "Unknown Column Name" ...] si on insère une valeur pour un nom de colonne inconnu
   - [Error ""Violated Not Null Constraint"" ...] si on a violé une contrainte Not_null
   - [Error ""Violated Unique Constraint"" ...] si on a violé une contrainte Unique

Notez qu'on ne peut pas avoir deux valeurs Null pour une colonne ayant la contrainte Unique.
(contrairement aux ANSI standards ->
'a UNIQUE constraint must disallow duplicate non-NULL values but accept multiple NULL values')

  val insert : values_to_insert -> table -> table
*)
let insert values (sc,pk,a,lct) =

  let new_id =
    try
      List.assoc pk values
    with Not_found -> raise (Error("PK not inserted","A value for the primary key column is needed."))
  in

  if Arbre.mem new_id a
  then raise (Error("PK integrity error","The value '"^new_id^"' has already been used as primary key."))
  else

    let new_line =
      List.fold_left
	(
	  fun line (cn,value) ->
	    if List.mem cn sc
	    then Arbre.add cn (Value(value)) line
	    else raise (Error("Unknown Column Name","incorrect column name : "^cn))
	)
	Arbre.empty
	values
    in
    
    if
      List.fold_left
	(
	  fun acc (ct_nom,ct) ->
	    match ct with
	    | Not_null(cn) ->
	       if Arbre.mem cn new_line
	       then true
	       else raise (Error("Violated Not Null Constraint",ct_nom))
	    | Unique(sco) ->
	       let (sco,result) =
		 let restrictions =
		   List.map
		     (
		       fun cn ->
			 let a_value =
			   try
			     Arbre.find cn new_line
			   with Not_found -> Null
			 in
			 match a_value with
			 | Null -> cn,IsNull
			 | Value(v) -> cn,Equal(v)
		     )
		     sco
		 in
		 select (Some(sco)) (datas (sc,pk,a,lct)) restrictions
	       in
	       if List.length result = 0
	       then true
	       else raise (Error("Violated Unique Constraint",ct_nom))
	)
	true
	lct
    then

      let a = Arbre.add new_id new_line a in
      sc,pk,a,lct

    else raise Impossible

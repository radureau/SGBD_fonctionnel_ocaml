type key = string
  
type +'a t

(*
@requires
@ensures
  retourne un arbre prefixe associatif vide
  la clef est de type string
  le type des valeurs sera défini au premier appel de Arbre.add
@raises
*)
val empty: 'a t

(*
@requires
@ensures 'Arbre.is_empty empty' retourne true, false sinon
@raises
*)
val is_empty: 'a t -> bool

(*
@requires
@ensures retourne true si l'arbre contient un association pour la clé, false sinon
@raises
*)
val mem:  key -> 'a t -> bool

(*
@requires
@ensures
  ajoute l'association clé->valeur à l'arbre
  si une valeur était déjà associée à la clé dans l'arbre, la nouvelle association écrase la précédante
@raises
*)
val add: key -> 'a -> 'a t -> 'a t

(*
@requires
@ensures
  retire l'association clé->valeur de l'arbre
  si la clé ne se trouvait pas dans l'arbre, aucune exception n'est levée
@raises
*)
val remove: key -> 'a t -> 'a t

(*
@requires
@ensures  execute un fold avec une fonction aux différentes associations clé->valeur de l'arbre sans relation d'ordre
@raises
*)
val fold: (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b

(*
@requires l'arbre contient une association clé->valeur pour la clé
@ensures retourne la valeur associée à la clé dans l'arbre
@raises Not_found si l'arbre ne contient aucune association clé->valeur pour la clé
*)
val find: key -> 'a t -> 'a

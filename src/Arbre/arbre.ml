module Pmap = Map.Make(Char)

(*
@requires
@ensures retourne la liste de char dont est composée la chaine de caractères
@raises 

val explode: str -> char list
*)
let explode s =
  let rec aux l i =
    if i<0
    then l
    else aux (s.[i]::l) (i-1) in
  aux [] (String.length s - 1)

(*
@requires
@ensures retourne la chaine de caractères composée des caractères de la liste
@raises

val implode: char list -> str
*)
let implode l = String.concat "" (List.map Char.escaped l)

type key = string

type 'a t = T of 'a option * ('a t) Pmap.t

(*
@requires
@ensures
  retourne un arbre prefixe associatif vide
  la clef est de type string
  le type des valeurs sera défini au premier appel de Arbre.add
@raises
*)
let empty = T(None, Pmap.empty)

(*
@requires
@ensures 'Arbre.is_empty empty' retourne true, false sinon
@raises
*)
let is_empty t = (=) empty t

let rec l_mem l (T(v,m)) =
  match l with
  | [] -> v!=None
  | c::l' ->
     try
       let t' = Pmap.find c m in
       l_mem l' t'
     with Not_found -> false

(*
@requires
@ensures retourne true si l'arbre contient un association pour la clé, false sinon
@raises
*)
let mem key t = l_mem (explode key) t

exception Already_in

let l_add l value t =
  let rec aux l (T(v,m)) =
    match l with
    | [] ->
       (match v with
       | Some(v') ->
	  if v'=value
	  then raise Already_in
	  else T(Some(value),m)
       | None -> T(Some(value),m)
       )
    | c::l ->
       let t' =
	 try
	   Pmap.find c m
	 with Not_found -> empty
       in
       let t'' = aux l t' in
       let m' = Pmap.add c t'' m in
       T(v,m')
  in
  try
    aux l t
  with Already_in -> t

(*
@requires
@ensures
  ajoute l'association clé->valeur à l'arbre
  si une valeur était déjà associée à la clé dans l'arbre, la nouvelle association écrase la précédante
@raises
*)
let add key value t = l_add (explode key) value t

let rec l_remove l (T(v,m)) =
  match l with
  | [] -> T(None,m)
  | c::l' ->
     try
       let t' = Pmap.find c m in
       let t'' = l_remove l' t' in
       let m' =
	 if is_empty t''
	 then Pmap.remove c m
	 else Pmap.add c t'' m
       in T(v,m')
     with Not_found -> (T(v,m))

(*
@requires
@ensures
  retire l'association clé->valeur de l'arbre
  si la clé ne se trouvait pas dans l'arbre, aucune exception n'est levée
@raises
*)
let remove key t = l_remove (explode key) t

exception Impossible
  
(*
@requires
@ensures  execute un fold avec une fonction aux différentes associations clé->valeur de l'arbre sans relation d'ordre
@raises
*)
let fold f t acc =
  let rec aux rev_path (T(v,m)) acc' =
    let new_acc =
      if v!=None
      then
	match v with
	| Some(value) -> f (implode (List.rev rev_path)) value acc'
	| _ -> raise Impossible
      else acc'
    in
    Pmap.fold
      (fun c tc acc ->
	aux (c::rev_path) tc acc
      )
      m
      new_acc
  in
  aux [] t acc

let rec l_find l (T(v,m)) =
  match l with
  | [] ->
     (match v with
     | None -> raise Not_found
     | Some(value) -> value
     )
  | c::l' ->
     let t' = Pmap.find c m in
     l_find l' t'

(*
@requires l'arbre contient une association clé->valeur pour la clé
@ensures retourne la valeur associée à la clé dans l'arbre
@raises Not_found si l'arbre ne contient aucune association clé->valeur pour la clé
*)
let find key t = l_find (explode key) t
